ros-perception-pcl (1.7.5-2) unstable; urgency=medium

  * Add missing dependency
  * Add versioned dependencies on libpcl-dev

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 14 May 2024 09:30:19 +0200

ros-perception-pcl (1.7.5-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062867

 -- Benjamin Drung <bdrung@debian.org>  Thu, 29 Feb 2024 23:06:13 +0000

ros-perception-pcl (1.7.5-1) unstable; urgency=medium

  * New upstream version 1.7.5
  * Drop upstreamed patches

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 23 Jan 2023 08:18:39 +0100

ros-perception-pcl (1.7.4-4) unstable; urgency=medium

  * unchaged rebuild for new PCL

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 11 Jan 2023 07:37:54 +0100

ros-perception-pcl (1.7.4-3) unstable; urgency=medium

  * Add patches for PCL 1.13
  * Bump policy version (no changes)
  * Move package description to source package

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 06 Jan 2023 08:28:57 +0100

ros-perception-pcl (1.7.4-2) unstable; urgency=medium

  * Add patch for new pluginlib
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 13 Jun 2022 19:44:25 +0200

ros-perception-pcl (1.7.4-1) unstable; urgency=medium

  * New upstream version 1.7.4
  * Rebase patches

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 11 Feb 2022 09:41:03 +0100

ros-perception-pcl (1.7.3-3) unstable; urgency=medium

  * Add patch for new log4cxx (Closes: #1004781)

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 01 Feb 2022 22:35:57 +0100

ros-perception-pcl (1.7.3-2) unstable; urgency=medium

  * Enable tests

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 28 Oct 2021 08:15:26 +0200

ros-perception-pcl (1.7.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.7.3

 -- Timo Röhling <roehling@debian.org>  Mon, 27 Sep 2021 21:18:31 +0200

ros-perception-pcl (1.7.2-5) unstable; urgency=medium

  * Team upload.
  * Upload to unstable

 -- Timo Röhling <roehling@debian.org>  Wed, 22 Sep 2021 22:43:29 +0200

ros-perception-pcl (1.7.2-4) experimental; urgency=medium

  * Team upload.
  * Move pkg-config and CMake config files to /usr/lib/<triplet>
  * Bump Standards-Version to 4.6.0 (no changes)

 -- Timo Röhling <roehling@debian.org>  Wed, 22 Sep 2021 00:33:20 +0200

ros-perception-pcl (1.7.2-3) unstable; urgency=medium

  * simplify packaging

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 19 Dec 2020 18:06:45 +0100

ros-perception-pcl (1.7.2-2) unstable; urgency=medium

  * Add patch for Boost 1.74
  * bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 15 Dec 2020 13:21:22 +0100

ros-perception-pcl (1.7.2-1) unstable; urgency=medium

  * New upstream version 1.7.2
  * rebase patches

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 18 Oct 2020 16:35:10 +0200

ros-perception-pcl (1.7.1-3) unstable; urgency=medium

  * Add patch for missing include (Closes: #971125)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 27 Sep 2020 22:37:55 +0200

ros-perception-pcl (1.7.1-2) unstable; urgency=medium

  * Upload to unstable

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 30 Jun 2020 09:06:13 +0200

ros-perception-pcl (1.7.1-1) experimental; urgency=medium

  * Update d/watch to track ROS 1 releases
  * New upstream version 1.7.1
  * Update d/copyright
  * Fix lintian debian-rules-sets-DEB_BUILD_OPTIONS
  * Rebase patches and add one for PCL 1.11
  * bump debhelper version
  * Bump Sonames due to ABI changes

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 14 Jun 2020 21:07:02 +0200

ros-perception-pcl (1.7.0-2) unstable; urgency=medium

  * Fix MA hints
  * Fix spelling
  * Add workaround for full buildpath in generated files

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 10 Apr 2020 20:32:37 +0200

ros-perception-pcl (1.7.0-1) unstable; urgency=medium

  * Initial release. (Closes: #951736)

 -- Johannes 'josch' Schauer <josch@debian.org>  Thu, 20 Feb 2020 23:40:35 +0100
